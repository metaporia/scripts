#!/usr/bin/env bash
# muse.do : aggregate all 'do...' entries within ~/sputum/muse/*.*
for f in ~/sputum/muse/*.*; do echo -e "$f\n"; grep " do " $f; echo -e "\n"; done | less -color
