{
  description = "personal scripts";
  inputs = { nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable"; };

  outputs = { self, nixpkgs }:
    # TODO: 
    # - add package sets of scripts, e.g., hypr, exa-aliases, etc
    # - remove unused scripts
    # - decide on ./hypr/wl_raise_primary_web rewrite (re: global vs local summon)
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      writeShellApplication' = { name, runtimeInputs, text }:
        pkgs.writeShellApplication {
          inherit name runtimeInputs text;
          bashOptions = [ ];
        };
      wl_raise_x = writeShellApplication' {
        name = "wl_raise_x";
        # hyprland is a dep but it feels weird to pull it in if wl_raise_x is
        # accidentally included.
        runtimeInputs = with pkgs; [ getoptions jq ];
        text = builtins.readFile ./hypr/wl_raise_x;
      };

      wl_raise_primary_web = writeShellApplication' {
        name = "wl_raise_primary_web";
        # hyprland is a dep but it feels weird to pull it in if wl_raise_x is
        # accidentally included.
        runtimeInputs = with pkgs; [ firefox jq ];
        text = builtins.readFile ./hypr/wl_raise_primary_web;
      };

      raise_dots = writeShellApplication' {
        name = "raise_dots";
        runtimeInputs = with pkgs; [ tmux neovim ];
        text = builtins.readFile ./hypr/raise_dots;
      };

      import-env = writeShellApplication' {
        name = "import-env.sh";
        runtimeInputs = with pkgs; [ tmux ];
        text = builtins.readFile ./hypr/import-env.sh;
      };

      hypr = pkgs.buildEnv {
        name = "hypr";
        paths = [ raise_dots wl_raise_primary_web wl_raise_x import-env ];
        meta = {
          description = "Hyprland utils; mostly to summon various programs";
        };
      };

      nix-utils =
        with pkgs;
        stdenv.mkDerivation {
          name = "nix-utils";
          version = "0.1.0";
          src = self;
          installPhase = ''
            mkdir -p $out/bin;
            cd nix-utils
            cp -t $out/bin ./*
          '';
        };


      # misc bash one-liner scripts
      misc-bash-utils =
        with pkgs;
        stdenv.mkDerivation {
          name = "misc-bash-utils";
          version = "0.1.0";
          src = self;
          installPhase = ''
            mkdir -p $out/bin;
            cd misc-bash-utils/
            cp -t $out/bin ./*
          '';
        };

      eza-wrappers =
        with pkgs;
        stdenv.mkDerivation {
          name = "eza-wrappers";
          version = "0.1.0";
          src = self;
          installPhase = ''
            mkdir -p $out/bin;
            cd eza-wrappers
            cp -t $out/bin ./*
          '';
        };

      all' = pkgs.buildEnv {

        name = "scripts-all";
        paths = [ hypr eza-wrappers misc-bash-utils nix-utils ];

      };

      # hypr' = builtins.listToAttrs (map (p: { name = p.name; value = p; })
      #   [ raise_dots wl_raise_primary_web wl_raise_x ]);
    in
    {


      #inherit wl_raise_x wl_raise_primary_web raise_dots;

      packages.x86_64-linux.default = all';
      packages.x86_64-linux.all = all';
      packages.x86_64-linux.sets = {
        inherit hypr eza-wrappers misc-bash-utils nix-utils;
      };


      #overlays.default = self.overlays.scripts-complete;
      #overlays.scripts-complete = final: prev: {
      #  scripts-complete = self.packages.x86_64-linux.scripts-complete;
      #};
      nixosModules.scripts = { lib, config, ... }:
        with lib;
        let cfg = config.mine.scripts;
        in
        {
          options.mine.scripts = {
            enable = mkEnableOption "aporia's misc. scripts";
          };
          # only enable if hyprland is enabled 
          config = mkIf cfg.enable { };

        };

    };
}
